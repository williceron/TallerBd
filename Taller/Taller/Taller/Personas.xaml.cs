﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Taller
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Personas : ContentPage
	{
		public Personas ()
		{
			InitializeComponent ();
		}

        public void ButtonClickCreate(object sender, EventArgs e)
        {

            //   crear objeto 
            Persona persona = new Persona()
            {
                Name = name.Text,
                Phone = phone.Text,
                Email = email.Text,
                Sex = false
            };

            // conexion a la base de datos
            using (SQLite.SQLiteConnection connection = new SQLite.SQLiteConnection(App.urlBd))
            {
                // crear tabla en base de datos
                connection.CreateTable<Persona>();

                // crear registro en la tabla
                var result = connection.Insert(persona);

                if (result > 0)
                {
                    DisplayAlert("Correcto", "El usuario se creo correctamente", "OK");
                }
                else
                {
                    DisplayAlert("Incorrecto", "El usuario no fue creado", "OK");
                }
            }
        }


        async public void ListWindow(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Personas());
        }
    }
}