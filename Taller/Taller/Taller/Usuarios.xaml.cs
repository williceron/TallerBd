﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Taller
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Usuarios : ContentPage
	{
		public Usuarios ()
		{
			InitializeComponent ();
		}

        public void ButtonClick(object sender, EventArgs e)
        {

            //   crear objeto 
            Usuario usuario = new Usuario()
            {
                NameUser = nameUser.Text,
                password = password.Text,
                avatar = avatar.Text,
                state = false

            };

            // conexion a la base de datos
            using (SQLite.SQLiteConnection connection = new SQLite.SQLiteConnection(App.urlBd))
            {
                // crear tabla en base de datos
                connection.CreateTable<Usuario>();

                // crear registro en la tabla
                var result = connection.Insert(usuario);

                if (result > 0)
                {
                    DisplayAlert("Correcto", "El usuario se creo correctamente", "OK");
                }
                else
                {
                    DisplayAlert("Incorrecto", "El usuario no fue creado", "OK");
                }
            }
        }


        async public void ListUser(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Usuarios());
        }
    }
}
