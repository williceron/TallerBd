﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Taller
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
		}

        async public void ButtonClickPersons(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Personas());

        }

        async public void ButtonClickUsers(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Usuarios());
        }

    }
}
