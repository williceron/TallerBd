﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Taller
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ListarUsuarios : ContentPage
	{
		public ListarUsuarios ()
		{
			InitializeComponent ();
		}

        protected override void OnAppearing()
        {
            base.OnAppearing();

            // conexion a la base de datos
            using (SQLite.SQLiteConnection connection = new SQLite.SQLiteConnection(App.urlBd))
            {

                List<Usuarios> listaUsuarios;
                listaUsuarios = connection.Table<Usuarios>().ToList();

                listViewUsuarios.ItemsSource = listaUsuarios;

            }
        }
    }
}